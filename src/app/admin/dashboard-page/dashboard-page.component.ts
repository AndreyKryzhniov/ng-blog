import {Component, OnDestroy, OnInit} from '@angular/core';
import {Post} from '../../shared/types/interfaces';
import {PostsService} from '../../shared/posts.service';
import {Subscription} from 'rxjs';
import {AlertService} from '../shared/services/alert.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {

  public posts: Post[] = [];
  postSub: Subscription;
  delSub: Subscription;
  searchStr = ''

  constructor(private postsService: PostsService,
              private alert: AlertService) {
  }

  ngOnInit(): void {
    this.postSub = this.postsService.getAll().subscribe((posts) => {
      this.posts = posts;
    });
  }

  remove(id: string) {
    this.delSub = this.postsService.removePost(id).subscribe(() => {
      this.posts = this.posts.filter((post) => post.id !== id)
      this.alert.danger('Post was deleted')
    });
  }

  ngOnDestroy(): void {
    if (this.postSub) {
      this.postSub.unsubscribe();
    }
    if (this.delSub) {
      this.delSub.unsubscribe();
    }
  }

}
